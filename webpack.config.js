//引入路径
const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')


//配置导出项
module.exports = {

  //入口文件
  entry: './src/index.ts',

  //配置出口文件
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'bundle.js',
    environment: {
      arrowFunction: false //关闭webpack的箭头函数，可选(babel-core)
    }
  },

  // optimization: {
  //   minimize: false // 关闭代码压缩，可选
  // },

  resolve: {
    extensions: [".ts", ".js","less"]      //用到文件的扩展名
  },

  //配置编译文件
  module: {
    rules: [
      {
        test: /\.ts$/,
        use: [
          {
            loader: "babel-loader",
            options: {
              presets: [
                [
                  "@babel/preset-env",
                  {
                    "targets": {
                      "chrome": "58",
                      "ie": "11"
                    },
                    "corejs": "3",
                    "useBuiltIns": "usage"
                  }
                ]
              ]
            }
          },
          {
            loader: 'ts-loader'
          }
        ],
        exclude: /node_modules/
      },
      {
        test:/\.less$/,
        use:['style-loader','css-loader','less-loader']
      }
    ]
  },

  //插件使用
  plugins: [

    new CleanWebpackPlugin(),

    new HtmlWebpackPlugin({
      template: './src/index.html'
    }),

  ]

}

