import './static/index.less'


1.//基本类型
let a:number
a=5

let b:boolean
b=true
b=false

let c:string
c='hello ts'

let d:any
d=true

2.//注：最好不要用any类型，不仅消耗自己，而且消耗别人
let f:string
f=d

3.//unknown 再不确定类型的条件下
//有些情况下，变量的类型对于我们来说是很明确，但是TS编译器却并不清楚，此时，可以通过类型断言来告诉编译器变量的类型，断言有两种形式：

//第一种
let someValue: unknown = "this is a string";
let strLength: number = (someValue as string).length;

//第二种
let s: unknown = "this is a string";
let st: number = (<string>s).length;


4.//字面量
let j:'hellow'
j='hellow'

let l:11 | 'hellow'


5.//对象
let obj:{name:string,age:number}
obj={name:'张三',age:12}


let oobj:{[prop:string]:any} //对象中为任意类型
oobj={name:'王五',kl:12}


6.//数组类型
let arr1:number[]
arr1=[1,2,3,4]

let arr2:Array<string>
arr2=['1','2','3']

7.//tuple元祖类型
let tuple:[string,number]
tuple=['nihao',789]

8.//enum枚举类型
enum Gender{
  man=1,
  woman=2
}

let gender:Gender=Gender.man


9.//函数类型
let fn1:(name:string)=>string
//1）void没有返回值
  let fn2:(name:number)=>void
//2）never 表示永远也不会有返回值
  function fn3(name:12):never{
      throw new Error('错误了')
  }

10.//泛型
/*
function fun:any(params:any):any {
    return params
} 
函数有一个参数类型不确定，但是能确定的时其返回值的类型和参数的类型是相同的，
由于类型不确定所以参数和返回值均使用了any，但是很明显这样做是不合适的，
首先使用any会关闭TS的类型检查，其次这样设置也不能体现出参数和返回值是相同的类型
*/

function fun<T>(params:T):T {
    return params
} 

function test<T, K>(a: T, b: K): K{
  return b;
}

test<number, string>(10, "hello");


11.//对象属性修饰符











